import pytest

from .interpreter import Interpreter


def test_inc_ptr():
    interp = Interpreter()
    interp._inc_ptr()
    assert interp._ptr == 1
    assert len(interp._mem) == 2


def test_dec_ptr():
    interp = Interpreter()
    interp._inc_ptr()
    interp._dec_ptr()
    assert interp._ptr == 0


def test_dec_exception():
    interp = Interpreter()
    with pytest.raises(Exception):
        interp._dec_ptr()


def test_inc_val():
    interp = Interpreter()
    interp._inc_val()
    assert interp._mem[0] == 1


def test_dec_val():
    interp = Interpreter()
    interp._dec_val()
    assert interp._mem[0] == -1


def test_output():
    interp = Interpreter()
    for i in range(0, 97):
        interp._inc_val()

    interp._echo()
    assert interp._output == 'a'


def test_input():
    interp = Interpreter()
    interp._input = 'a'
    interp._read()
    assert interp._mem[0] == 97


def test_prog_1():
    interp = Interpreter()
    source = '++++>+++>++>+<-<--<---'
    interp.run(source)

    assert interp._mem[0] == 1
    assert interp._mem[1] == 1
    assert interp._mem[2] == 1
    assert interp._mem[3] == 1


def test_prog_2():
    interp = Interpreter()
    source = '+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++.'
    output = interp.run(source)

    assert interp._mem[0] == 97
    assert output == 'a'

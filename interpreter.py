class Interpreter:

    def __init__(self):
        self._input = ''
        self._output = ''
        self._mem = [0]
        self._ptr = 0

    def _inc_ptr(self):
        self._ptr += 1
        if self._ptr >= len(self._mem):
            self._mem.append(0)

    def _dec_ptr(self):
        self._ptr -= 1
        if self._ptr < 0:
            raise Exception(
                'self._ptr should not be negative. The value of self._ptr was %s' %
                (self._ptr))

    def _inc_val(self):
        self._mem[self._ptr] += 1

    def _dec_val(self):
        self._mem[self._ptr] -= 1

    def _echo(self):
        self._output += str(chr(self._mem[self._ptr]))

    def _read(self):
        self._mem[self._ptr] = ord(self._input[:1])

    def run(self, source, input=''):
        while len(source) > 0:
            switch = {
                '>': self._inc_ptr,
                '<': self._dec_ptr,
                '+': self._inc_val,
                '-': self._dec_val,
                '.': self._echo,
                ',': self._read
            }
            switch[source[:1]]()
            source = source[1:]

        return self._output
